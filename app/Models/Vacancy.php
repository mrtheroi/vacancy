<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Hashing\HashServiceProvider;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Http;
use SimpleXMLElement;

class Vacancy extends Model
{
    use HasFactory;

    public static function getVacancies(){
        $response = Http::get('https://people-pro.com/xml-feed/indeed');
        $xml = new SimpleXMLElement($response->body());

        foreach ($xml->job as $job) {
            // Obtener el valor del tag indeed-apply-data
            $applyData = (string) $job->{'indeed-apply-data'};

            // Parsear la cadena CDATA a un array
            parse_str(html_entity_decode($applyData), $applyDataArray);

            // Acceder al valor de indeed-apply-jobid
            $jobId = $applyDataArray['indeed-apply-jobid'];

            // Crear la estructura de datos de la vacante
            $vacancy = [
                'id' => (string) $jobId,
                'title' => (string) $job->title,
                'company' => (string) $job->company,
                'description' => (string) $job->description,
                'city' => (string) $job->city,
                'state' => (string) $job->state,
                'country' => (string) $job->country,
                'date' => Carbon::parse($job->date)->format('Y-m-d')
            ];

            $vacancies[] = $vacancy;
        }

        return collect($vacancies);

    }




}
