<?php

namespace App\Http\Livewire;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;
use App\Models\Vacancy as ModelVacancy;

class Vacancy extends Component
{
    public $open = false;

    public $title, $company, $description, $city, $state, $country, $date;
    public $selected_id;

    public function mount()
    {
        $this->selected_id = 0;
    }


    public function resetUI()
    {
        $this->title = '';
        $this->company = '';
        $this->description = '';
        $this->city = '';
        $this->state = '';
        $this->country = '';
        $this->date = '';
        $this->open = false;
    }

    public function updatingOpen()
    {
        if(!$this->open) {
            return;
        }
        $this->resetUI();
    }

    public function render()
    {
        $vacancies = ModelVacancy::getVacancies();
        $perPage = 10;
        $currentPage = request()->page ?? 1;

        $pagedData = $vacancies->slice(($currentPage - 1) * $perPage, $perPage)->all();

        $vacanciesObject = new LengthAwarePaginator(
            $pagedData,
            count($vacancies),
            $perPage,
            $currentPage,
            ['path' => route('vacancies')]
        );
        return view('livewire.vacancy.vacancy',compact('vacanciesObject'));
    }

    public function edit($id)
    {
        $vacancy = collect(ModelVacancy::getVacancies())->firstWhere('id', $id);
        if ($vacancy) {
            $this->title = $vacancy['title'];
            $this->company = $vacancy['company'];
            $this->description = $vacancy['description'];
            $this->city = $vacancy['city'];
            $this->open = true;
        }
    }
}
