
Livewire.on('alert-create', function(message){
    Swal.fire(
        'Good job!',
        message,
        'success'
    )
})

Livewire.on('alert-update', function(message){
    Swal.fire(
        'Good job!',
        message,
        'success'
    )
})

window.addEventListener('show-alert', event =>{

    Swal.fire({
        icon: 'warning',
        title: 'Confirmar',
        text: '¿Confirmas eliminar el registro?',
        type: 'warning',
        showCancelButton: true,
        cancelButtonText: 'Cerrar',
        cancelButtonColor: '#dc2626',
        confirmButtonColor: '#166534',
        confirmButtonText: 'Aceptar'

    }).then(function(result){
        if(result.value){
            Livewire.emit('deleteConfirmed')
            Swal.fire(
                'Buen trabajo',
                'Tu petición ha sido realizada con exito',
                'success',
            )
        }
    })
})
window.addEventListener('post-deleted', function(message) {

    Swal.fire(
        'Buen trabajo',
        message,
        'success',
    )
})
