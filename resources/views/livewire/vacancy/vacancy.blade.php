<div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Vacancies') }}
        </h2>
    </x-slot>
    <x-table>

        @if($vacanciesObject->count() > 0)

            <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                <tr>
                    <th scope="col" class="cursor-pointer px-6 py-3">
                        id
                    </th>
                    <th scope="col" class="cursor-pointer px-6 py-3" wire:click="order('title')">
                        Title
                    </th>
                    <th scope="col" class="cursor-pointer px-6 py-3" wire:click="order('company')">
                        Company
                    </th>
                    <th scope="col" class="cursor-pointer px-6 py-3" wire:click="order('date')">
                        Date
                    </th>
                    <th scope="col" class="px-6 py-3 text-center">
                        Action
                    </th>
                </tr>
                </thead>
                <tbody>
                @foreach ($vacanciesObject as $item)
                    <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 hover:bg-gray-50 dark:hover:bg-gray-600">
                        <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{$item['id']}}
                        </th>
                        <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{$item['title']}}
                        </th>
                        <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{$item['company']}}
                        </th>
                        <th scope="row" class="px-6 py-4 font-medium text-gray-900 whitespace-nowrap dark:text-white">
                            {{$item['date']}}
                        </th>
                        <td class="px-6 py-4 flex justify-center">
                            <a class="btn btn-green" wire:click="edit({{ $item['id'] }})">
                                <i class="fa-regular fa-pen-to-square"></i> Ver detalle d ela vacacante
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @if($vacanciesObject->lastPage() > 1)
            <div class="p-2">
                {{ $vacanciesObject->links() }}
            </div>
        @endif

        @else
            <div class="px-6 py-4">
                No existen registros que mostrar
            </div>
        @endif
    </x-table>
    @include('livewire.vacancy.form')
</div>

@section('script')
    <script src="{{ asset( 'js/vacancy.js' ) }}" defer></script>
@endsection
