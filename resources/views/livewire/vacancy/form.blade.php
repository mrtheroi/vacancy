<div>
    <x-dialog-modal wire:model="open">
        <x-slot name="title">
            {{ $selected_id != '' ? 'Revisa la vacante' : 'Crear una nueva entrada'}}
        </x-slot>

        <x-slot name="content">

            <div class="mb-4">
                <x-label value="Title" class="mb-2" />
{{--                <x-input type="text" wire:model="title" class="w-full" readonly/>--}}
                <h1>{{ $title }}</h1>

                <x-label value="Company" class="mb-2" />
                <h1>{{ $company }}</h1>

                <x-label value="Description" class="mb-2" />
                <h1>{{ $description }}</h1>

                <x-label value="City" class="mb-2" />
                <h1>{{$city}}</h1>

                <x-label value="State" class="mb-2" />
                <h1>{{$state}}</h1>

                <x-label value="Country" class="mb-2" />
                <h1>{{$country}}</h1>

                <x-label value="Date" class="mb-2" />
                <h1>{{$date}}</h1>
            </div>

        </x-slot>


        <x-slot name="footer">

            <x-secondary-button class="mr-2" wire:click="$set('open', false)">
                Cerrar
            </x-secondary-button>
        </x-slot>
    </x-dialog-modal>
</div>
